package com.example.myathtut.layout.activities;

import android.app.Application;
import android.content.Context;

/**
 * Created by myathtut on 6/30/16.
 */
public class LayoutApp extends Application {
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();

        context=getApplicationContext();
    }

    public static Context getContext() {
        return context;
    }
}
